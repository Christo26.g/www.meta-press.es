= Motivations
:slug: motivations
:lang: fr
:date: 2017-12-01


== Revue de presse

J'ai fait partie, dès 2008, des citoyens indépendants engagés dans le même sens
que La Quadrature du Net… Après quelques traductions, je me suis volontairement
attelé à une tâche besogneuse : la revue de presse ; pour pouvoir me sentir utile
en attendant de mieux saisir les rouages politiques complexes des parlements
français, et européen.

Pour y comprendre quelque chose, il fallait en passer par la lecture de
nombreux articles de presse, et j'avais là une sélection de choix, avant que ce
ne soit, quelques années plus tard, mes choix qui fassent la sélection…

La revue de presse, c'était beaucoup de travail et un engagement constant
pour rester à jour. Il y avait une petite partie du travail qui était
intéressante : lire des articles, choisir ceux à retenir, en extraire les
bonnes lignes… l'autre partie, malheureusement plus imposante, était des plus
rebutante : il fallait copier/coller le titre, copier/coller la date (et où ils
l'ont planqué dans leur page !?), copier/coller un extrait, copier/coller le
lien… Cette mécanique est bien plus chronophage que la lecture de l'article elle même.

Pendant plus de 5 ans, j'ai consacré la majorité de mon temps libre à tenir la
revue de presse de la Quadrature à jour. Des milliers d'articles citaient le
porte-parole de l'association footnote:[Je me souviens notamment de mon premier
New York Times. Je découvrais ce journal américain dans le lobby d'un hôtel
finlandais, en week-end… Sur plusieurs colonnes à la Une s'étalait une
interview du porte-parole ! Photo, email, notation, transcription… un peu plus
tard dans la nuit ce fut publié, je n'avais pas sommeil de toutes façons.],
parfois d'un simple copier/coller, parfois d'une belle analyse, parfois en
distordant à dessein ses propos… Et il fallait aller chercher plus loin ! Comme
le dit solennellement une fois Jérémie Zimmermann à la petite équipe que je
peinais à motiver ce soir là : nous étions ses yeux.

J'avais aussi l'impression d'être la voiture balai du collectif, à la traine,
loin derrière. Le poste était pourtant stratégique, il n'y avait pas meilleur
école et il avait également une importance stratégique : il fallait populariser
nos domaines de lutte, pourtant complexes et abstraits. En effet, la neutralité
du net n'apparait qu'indirectement comme étant un canal de la liberté
d'expression, qui elle même n'apparait qu'indirectement comme une condition
nécessaire à une société juste.

Il fallait aussi augmenter la résonance de nos échos dans la presse, et avec
la Quadrature, les choses allaient encore plus loin… en effet, au delà des
raisonnements rationnels, la revue de presse mis également en avant le facteur
humain du journalisme, car il est arrivé qu'une belle analyse citant la
Quadrature manque à l'appel, et qu'à l'interview suivante, les journalistes
s'en plaignent, ronchons, déçus de ne pas avoir été trouvés, ou retenus.


== Moteur de recherche spécialisé

La tâche était colossale. Nous nous partagions les sources nationales et
internationales à scruter quotidiennement. Je trouvais des heures d'occupation
à chaque nouveau bénévole. Tiraillés entre le besoin d'être à la hauteur de
l'enjeu, et celui d'être indépendant et cohérent avec nos propos, nous avons
finalement mis en place des alertes _Google News_ dans une période de disette
de forces militantes.

J'ai longuement cherché une alternative à l'outil, mais n'ai rien trouvé qui
soit libre, ou même accessible. Et comment rivaliser avec le pouvoir
d'indexation d'un des plus gros moteurs de recherche ? Comment se passer de
leur index ? Ma grand-mère avait la solution elle : « Mieux vaut 1 qui sait,
que 10 qui cherchent. » footnote:[Ou encore : « il y a 10 catégories de
personnes, ceux qui savent (compter en binaire), et les autres… »]

Pour avoir nos réponses, sans passer par un oracle central, on peut donc se
cantonner à demander à ceux qui savent… Et en l'occurrence celui qui sait quels
articles ont été publiés aujourd'hui, c'est bien le journal qui les a publiés.
Unir, dans une même requête, les index des journaux, c'est un peu comme broder
un moteur de recherche réparti… Chacun indexe son contenu, et le cachet de la
poste fait foi.

Oui, car ce qui fait la pertinence des résultats, concernant la presse, c'est
leur date. Or là-dessus, Google News n'apporte pas, dans ses pages, une grande
valeur ajoutée à l'ordre chronologique. Ordonner des résultats par date, c'est
à la portée de n'importe quel ordinateur.


== Considérations matérielles et techniques, persévérance

J'ai commencé un premier prototype fin 2013. Il était trop lent et le
navigateur ne permettait pas de faire partir les requêtes depuis le poste
de l'utilisateur… J'ai rapidement eu d'autres fusées à coder en JavaScript,
puis trop de distractions pour finir de creuser le sujet. Mais j'ai gardé
l'idée en tête tout ce temps, Benjamin Bayart m'avait dit droit dans les yeux
que se passer des moteurs de recherche centralisés est un enjeu majeur du
logiciel libre.

Jusqu'à cet été, où un ami (Taziden) est venu me déranger un soir avec un bout
de code "JavaScript" pour gérer ses playlists… Moi, l'expert en JavaScript, à qui
l'on demandait conseil, j'ai ouvert de grands yeux, ne reconnaissant pas les
constructions syntaxiques introduites dans le langage à la faveur d'une mise à
jour majeure de la norme. Après 20 ans d'immobilisme et une petite décennie
d'avancées disparates de navigateurs, la nouvelle norme (ECMAScript 5/6/7)
offrait beaucoup de nouvelles possibilités, que j'avais le loisir d'explorer,
étant devenu associé-gérant entre temps.

Avec l'apparition de Firefox, le web était redevenu, il y a dix ans, un terrain
d'innovation. Or, c'est notamment sur le JavaScript, ses fonctionnalités et sa
vitesse d'exécution, que les navigateurs issus de cette ouverture (Chrome,
IE7-8-9…) ont choisi de se mesurer. De simple outil de control de
formulaire, le JavaScript est devenu un langage complet, et a connu la plus
grosse accélération de la décennie. Plusieurs firmes ont même tenté de bâtir
des systèmes d'exploitation complet sur ce langage (Palm avec son WebOS,
Mozilla avec FirefoxOS).

On comprend alors plus facilement pourquoi Mozilla a décidé de baser les
extensions de son nouveau navigateur sur cette technologie, quitte à se
débarrasser des précédentes réalisations. Car oui, aujourd'hui, une WebExtension
pour Firefox, Chromium ou Opera, ça peut se résumer à un fichier JavaScript. Et
un JavaScript doté de super-pouvoirs. L'installation d'une extension étant
considérée comme un consentement fort de la part de l'utilisateur, il devient
possible d'accéder au contenu des _iframes_ et donc de faire partir les
requêtes depuis le navigateur, au lieu d'avoir à passer par un serveur
centralisant l'usage.

3 mois plus tard, un premier prototype était capable de découvrir des millions
de résultats en moins de dix secondes, et en rapatriait des centaines sur
l'ordinateur de l'utilisateur : les 10 derniers de chacun des 30 journaux
interrogés dans 24 pays différents. Un tour du monde de la presse anglophone,
en un clic.

Voici une capture un peu plus récente et mieux habillée de l'outil.

image::/images/20171216_meta-press_country_640.png[link="/images/20171216_meta-press_country.png", title="Capture d'écran de 2017/12/16, cliquez pour agrandir"]
