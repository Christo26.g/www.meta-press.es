= gettext_html_auto.js : translation of WebExtension pages
:slug: gettext_html_auto_translation_of_webextension_pages
:lang: en
:date: 2020-05-25
:author: Siltaar

The "official" way of translating WebExtensions content is the `i18n` API,
which requires to define a key for each portion of text to translate.

This can be seen as brain consuming, especially in the case of a big web page,
with lots of text parts, like a form. It's the case of Meta-Press.es main
interface, but also for its preferences, like a lot of other WebExtensions.

In order to get close of the standard GNU `gettext` way of translating
programs, I decided to use Guillaume Potier's
https://github.com/guillaumepotier/gettext.js/[gettext.js] and to code the
helper functions that was missing to:

- collect texts needing translation (`xgettext`),
- automatically maintain a `template.json` and corresponding translations,
- and translate texts of HTML pages at loading time.

Now Meta-Press.es is available in English, French, Spanish and Esperanto !

To contribute a new language, you have just to fill the blanks in the
https://framagit.org/Siltaar/meta-press-ext/-/blob/master/html_locales/template.json[templace.json]
file of the Meta-Press.es project, and create a pull-request (or send it via
email).

If you're looking for a simple way of translation / internationalisation for
your Firefox Addon project (or a least your option page), I shared the
dedicated code as a separate project in the
https://framagit.org/Siltaar/gettext_html_auto.js[gettext_html_auto.js]
repository, along with some documentation. You would be welcome to use it.

With https://framagit.org/Siltaar/month_nb[month_nb], it's the 2nd side project
made for Meta-Press.es.
