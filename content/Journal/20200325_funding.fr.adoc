= Financement de la fondation NLnet
:slug: funds-from-the-nlnet-foundation
:lang: fr
:date: 2020-03-25
:author: Siltaar

Avec la sortie de la v1.0 et les bonnes statistiques du lancement, j'ai à
nouveau candidaté auprès des fonds m'ayant répondu
link:/journal/2019/funds-from-the-wau-holland-foundation.html[la dernière fois] :

- Mozilla via the MOSS
- the NLnet foundation
- the Open Society Institute (OSI)
- the OpenTech Fund (OTF)

Et la NLnet a répondu positivement !

La https://nlnet.nl[NLnet] est membre du consortium NGI Zero regroupant
plusieurs associations européenes s'occupant de simplifier les démarches
administrative du programme européen
https://ec.europa.eu/programmes/horizon2020/[Horizon 2020] pour la recherche et
l'innovation. De plus, NGI Zero regroupe un certain de compétences en
développement logiciel, permettant d'aider mieux les projets retenus qu'avec
simplement de l'argent.

Le processus de financement de la NLnet est simple et efficace. Il comporte peu
d'étapes, mais ces dernières se sont étalées sur 4 mois. Pour commencer, ils
étudient vraiment les candidatures et reviennent vers vous avec des questions
techniques précises si votre projet entre dans les domaines financés.

Si votre projet est fonctionnel et a besoin d'une amélioration atteignable, et
s'il présente suffisamment d'intérêt pour prendre la place d'une projet dans la
file d'attente, le projet est présenté à la Commission Européenne pour
validation.

Ensuite un petit dialogue s'installe avec la NLnet pour établir un _Memorandum
of Understanding_, un document décrivant entre autre les étapes du
développement que vous comptez mener pour le projet et les gratifications
associées. Lorsqu'une étape est ensuite atteinte, il suffit de l'annoncer à la
NLnet qui verse alors le financement correspondant. Pas d'autres complications
administratives, c'est un don charitable.

Voici les étapes du _Memorandum of Understanding_ 2019-12-019 de Meta-Press.es :

- *Automatisation de recherches :*
** automatisation de recherches (quotidiennement par exemple) https://framagit.org/Siltaar/meta-press-ext/-/issues/31[#31]
** procédure de test des sources pour découvrir lesquelles ne fonctionnent plus
- *Amélioration de l'interface :*
** interface traduisible (et traduction française) https://framagit.org/Siltaar/meta-press-ext/-/issues/30[#30]
** préférences utilisateur : https://addons.mozilla.org/fr/firefox/addon/meta-press-es/reviews/1482250/?src=search[fond sombre], chargement des gros titres (ou pas), nombre de résultats lus par sources https://framagit.org/Siltaar/meta-press-ext/-/issues/17[#17], https://framagit.org/Siltaar/meta-press-ext/-/issues/39[rechargement automatique d'une recherche] à chaque changement de critère de filtrage des sources…
** liens directs pour : exprimer un retour utilisateur https://framagit.org/Siltaar/meta-press-ext/-/issues/37[#37], créer une nouvelle source, remettre les filtres en configuration par défaut https://framagit.org/Siltaar/meta-press-ext/-/issues/38[#38]
- *Intégration avec le navigateur :*
** demander moins de permissions https://framagit.org/Siltaar/meta-press-ext/-/issues/33[#33]
** notifications quand les résultats sont disponibles (pour les longues recherches) https://framagit.org/Siltaar/meta-press-ext/-/issues/20[#20]
- *Plus de sources :*
** doubler le nombre de sources (120 -> 240)
** en incluant les https://en.wikipedia.org/wiki/Newspapers_of_record[_Newspapers of Record_] (version anglaise de la page Wikipédia)
- *Audits :*
** audit d'accessibilité
** audit de sécurité

L'enveloppe de financement prévue est de 20k€.

La https://www.wauland.de/en/[Wau Holland Foundation] a également alloué 1k€
cette année.

*Mon principal objectif est de passer Meta-Press.es de son état fonctionnel
actuel, à un état permettant de le maintenir sur le long terme.*

L'extension Meta-Press.es a été téléchargée 2000 fois au cours des 3 premiers
mois de son lancement et est toujours utilisé par plus de 800 utilisateurs par
jours selon Mozilla.
