= [DecodeProject.eu] Conf&eacute;rence technique et d&eacute;monstrations : Meta-Press.es
:slug: decode_project_eu_3rd_symopsium
:lang: fr
:date: 2019-11-05
:author: Siltaar

https://decodeproject.eu/events/exhibitors[*Exhibitors and Tech talk : Meta-Press.es*]

Explore la presse depuis ton ordinateur via Meta-Press.es, sans intermédiaire
entre les journaux et ton ordinateur. Découvre des millions de résultats en
quelques secondes, explore les derniers publiés par chaque journaux directement
dans Firefox via cette extension. Il est également possible de filtrer les
résultats obtenus (par journal, date ou sous-requête) et d'en sélectionner
certains pour établir une revue de presse, exportée au format RSS.

Meta-Press.es est un logiciel libre visant à être une alternative à
Google News pour les journalistes et les associations. L'extension est
développée par Simon Descarpentries (membre de la Quadrature du Net et artisan
du web), ainsi que déjà quelques contributeurs. Le projet est soutenu par la
_Wau Holland Foundation_. Toute contribution sera bienvenue.

https://decodeproject.eu/events/exhibitors
