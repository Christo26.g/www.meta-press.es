# Translations template for PROJECT.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-09-05 16:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: /home/siltaar/Boulot/meta-press/www.meta-press.es/theme/pelican-mp/templates/index.html:8
msgid ""
"Explore the press from your computer, with no middlemen between the "
"newspapers and you."
msgstr ""

#: /home/siltaar/Boulot/meta-press/www.meta-press.es/theme/pelican-mp/templates/index.html:9
msgid ""
"Discover millions of results within seconds, and explore the last ones in"
" Firefox via this addon."
msgstr ""

#: /home/siltaar/Boulot/meta-press/www.meta-press.es/theme/pelican-mp/templates/index.html:10
msgid "Select your press review and export it in one click."
msgstr ""

